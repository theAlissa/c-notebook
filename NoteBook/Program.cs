﻿using System;
using System.Collections;

namespace NoteBook
{
    
    class NoteBook
    {
        public static ArrayList notes = new ArrayList();
        public static bool isNotClosed = true;
        static void Main(string[] args)
        {
            showInfo();
            showCommands();
            while (isNotClosed)
            {
                Console.WriteLine("Введите команду:");
                Menu();
            }   
        }
        public static void Menu()
        {
            switch (Console.ReadLine())
        {
            case "1":
                AddNote();
                break;
            case "2":
                        ShowAllNotes();
                    if (notes.Count != 0)
                    {

                        try
                        {
                            Console.WriteLine("Введите номер записи, которую хотите изменить");
                            int num = Convert.ToInt32(Console.ReadLine()) - 1;
                            ChangeNote((Note)notes[num]);
                        }
                        catch
                        {
                            Console.WriteLine("Такого номера не существует");
                        }
                    }
                break;
                case "3":
                    ShowAllNotes();
                    if (notes.Count!= 0)
                    {
                        ShowAllNotes();
                        Console.WriteLine("Введите номер записи, которую хотите удалить");
                        try
                        {
                            int num = Convert.ToInt32(Console.ReadLine()) - 1;
                            DeleteNote((Note)notes[num]);
                        }
                        catch
                        {
                            Console.WriteLine("Такого номера не существует");
                        }
                    }
                    break;
                case "4":
                    PrintAllNotes();
                    break;
                case "5":
                    ShowAllNotes();
                    break;
                case "6":
                    showCommands();
                    break;
                case "7":
                    showInfo();
                    break;
                case "8":
                    Close();
                    break;
                default:
                Console.WriteLine("Такого номера нет");
                break;
                
        }
        }
        public static void Close()
        {
            isNotClosed = false;
            Console.WriteLine("Вы закрыли записную книжку. До новых встреч!");
        }
        public static void ChangeNote(Note n)
        {
            Print(n);
            Console.WriteLine("Введите номер поля, которое вы хотите изменить");
            string s = Console.ReadLine();
            while ( s!= "OK")
            {
                switch (s)
                {
                    case "1":
                        n.changeName();
                        break;
                    case "2":
                        n.changeSurname();
                        break;
                    case "3":
                        n.changeThirdName();
                        break;
                    case "4":
                        n.changeNumber();
                        break;
                    case "5":
                        n.changeCountry();
                        break;
                    case "6":
                        n.changeDateOfBirth();
                        break;
                    case "7":
                        n.changeCompany();
                        break;
                    case "8":
                        n.changeJobPossition();
                        break;
                    case "9":
                        n.changeOther();
                        break;
                }
                Print(n);
                Console.WriteLine("Введите OK чтобы сохранить изменения или ввыедите номер поля, которое вы хотите изменить");
                s = Console.ReadLine();
            }
        }
        public static void DeleteNote(Note n)
        {
            notes.Remove(n);
            Console.WriteLine("Запись успешно удалена.");
        }
        public static void showInfo()
        {
            Console.WriteLine("Привет! Я записная книжка. Количество записей во мне: {0}", notes.Count);
        }
        public enum Commands
        {
            Add=1,
            Change,
            Delete,
            PrintAll,
            ShowAll,
            showCommands,
            showInfo,
            close
        }

        public static void showCommands()
        {
            Console.WriteLine();
            Console.WriteLine("Список доступных команд: ");
            for (int i = 1; i < Enum.GetNames(typeof(Commands)).Length+1; i++)
            {
                Console.WriteLine(i+") "+(Commands)i+";");
            }
            Console.WriteLine("Чтобы выбрать команду введите ее номер ");
        }
        public static void ShowAllNotes()
        {
            if (notes.Count > 0)
            {
                Console.WriteLine();
                for (int i = 0; i < notes.Count; i++)
                {
                    Console.Write(i + 1 + ".");
                    ShowNote((Note)notes[i]);
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Во мне пока нет записей");
            }
        }
        public static void PrintAllNotes()
        {
            showInfo();
            for (int i = 0; i < notes.Count; i++)
            {
                Console.Write(i+1 + ".");
                Print((Note)notes[i]);
                Console.WriteLine();
            }

        }
        public static void AddNote()
        {
            notes.Add(new Note());
        }
        public static void ShowNote(Note hum)
        {
            Console.WriteLine("Имя: {0}\nФамилия: {1}\nНомер телефона: {2}", hum.name, hum.surname,hum.number);
        }
        public static void Print(Note hum)
    {
            Console.WriteLine("1)Имя: {0}\n2)Фамилия: {1}\n3)Отчество: {2}\n4)Номер телефона: {3}\n5)Страна: {4}\n6)Дата рождения: {5}\n7)Организация: {6}\n8)Должность: {7}\n9)Прочие заметки: {8}", hum.name, hum.surname,hum.thirdName, hum.number,hum.country,hum.dateOfBirth,hum.company,hum.jobPos,hum.other);
    }
    }  
}
