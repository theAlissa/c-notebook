﻿using System;


public class Note
{
    public string name;
    public string surname;
    public string thirdName;
    public string number;
    public string country;
    public string dateOfBirth;
    public string company;
    public string jobPos;
    public string other;

    public void changeName()
    {Console.WriteLine("Введите имя");
        name = input();
    }
    public void changeSurname()
    {
        Console.WriteLine("Введите фамилию");
        surname = input();
    }
    public void changeThirdName()
    {
        Console.WriteLine("Введите отчество");
        thirdName=nonNes();
    }
    public void changeCountry()
    {
        Console.WriteLine("Введите страну");
        country=input();
    }
    public void changeNumber()
    {
 Console.WriteLine("Введите номер телефона");
        number=inpInt();
    }
    public void changeDateOfBirth()
    {
Console.WriteLine("Введите дату рождения");
        dateOfBirth=inpDate();
    }
    public void changeCompany()
    {
        Console.WriteLine("Введите организацию");
        company=nonNes();
    }
    public void changeJobPossition()
    {
 Console.WriteLine("Введите должность");
        jobPos = nonNes();
    }
    public void changeOther()
    {
Console.WriteLine("Дополнительно");
        other=nonNes();
    }
        
   
    public Note()
    {
        Console.WriteLine("Создаем запись");
        
        changeName();
        changeSurname();
        changeThirdName();
        changeCountry();
        changeNumber();
        changeDateOfBirth();
        changeCompany();
        changeJobPossition();
        changeOther();
           
       
        Console.WriteLine("Человек создан");
    }
    
    public static string input()
    {
        string s = Console.ReadLine();
        while (s == " " | s.Length==0)
        {
            Console.WriteLine("Ввод некоректен");
            s = Console.ReadLine();
        }
        return s;
    }
    public static string nonNes()
    {
        return Console.ReadLine();
    }
    public static string inpInt()
    {
        string s= Console.ReadLine();
        bool isNumber = true;
        if (s.Length == 0)
        {
            isNumber = false;
        }
        if (isNumber)
        {
            if (s[0] != '+')
            {
                s = "+" + s;
            }
            for (int i = 1; i < s.Length; i++)
            {
                if (!Char.IsDigit(s[i]))
                {
                    isNumber = false;
                    break;
                }
            }
        }
        if (isNumber)
        {
            return s;
        }
        else
        {
            Console.WriteLine("Номер некорректен, попробуйте еще раз");
            return inpInt();
        }
        
    }
    public static string inpDate()
    {
        string s = Console.ReadLine();
        try
        {
            return DateTime.Parse(s).ToShortDateString();
        }
        catch
        {
            return s;
        }
       
    }
}

